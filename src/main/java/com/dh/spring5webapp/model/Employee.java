package com.dh.spring5webapp.model;

import javax.persistence.Entity;

@Entity
public class Employee {
    private String firstName;
    private String lastName;
    private Contract contract;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
