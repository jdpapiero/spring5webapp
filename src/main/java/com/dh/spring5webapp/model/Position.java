package com.dh.spring5webapp.model;

import javax.persistence.Entity;

@Entity
public class Position {
    public String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
